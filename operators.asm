handle_char:
    li $t4, 61                          # ascii for '='
    beq $t1, $t4, equals
    nop

    li $t4, 45                          # ascii for '-'
    beq $t1, $t4, handle_operator
    nop

    li $t4, 43                          # ascii for '+'
    beq $t1, $t4, handle_operator
    nop

    li $t4, 42                          # ascii for '*'
    beq $t1, $t4, handle_operator
    nop

    # if none of those, invalid character
    j invalid_char_error
    nop

    handle_operator:

        # if already have a previous operator, check if its an error
        li $t4, 1
        beq $s7, $t4, check_subtraction_sign
        nop

        # If there's a negative flag, set current integer to negative
        beq $s5, $t4, set_value_to_negative
        nop

        # if first operator not assigned yet
        beq $t2, $zero, store_operator
        nop

        # From this point on, any work will be done on the value
        # of the last operator
        move $t5, $s6                       # store temp value

        operations:
            li $t4, 45                          # ascii for '-'
            beq $t2, $t4, subtraction
            nop

            li $t4, 43                          # ascii for '+'
            beq $t2, $t4, addition
            nop

            li $t4, 42                          # ascii for '*'
            beq $t2, $t4, multiplication
            nop

            # return back
            jr $ra
            nop

set_value_to_negative:
    nor $s6, $s6, $s6	# NOT result
    addu $s6, $s6, $t4	# add 1 to result
    li, $s5, 0          # reset negative value flag
    j handle_operator
    nop

check_subtraction_sign:
    li $t4, 45
    bne $t1, $t4, invalid_expression_error
    nop

    j set_flag_to_negative
    nop

subtraction_logic:
    li $t2, 43
    j set_value_to_negative
    nop

addition_logic:
    li $t2, 45
    j set_value_to_negative
    nop

multiplication_logic:
    srl $t9, $s6, 31 	#get sign bit

    mullo $t5, $s6, $a0

    slt $t4, $t5, $zero
    bne $t4, $zero, output_overflow_error

    j valid_operator
    nop

#>> Arithmetic

subtraction:
    # if the value being subtracted is negative it's just an addition,
    # so flip its sign, make operator addition, reloop
    slt $t4, $s6, $zero
    bne $t4, $zero, subtraction_logic
    nop

    beq $s6, $zero, valid_operator
    nop

    # if new > old, value overflowed
    subu $t5, $a0, $s6
    slt $t4, $t5, $a0
    beq $t4, $zero, output_overflow_error
    nop

    j valid_operator
    nop

addition:
    # if the value being added is negative it's just a subtraction,
    # so flip its sign, make operator subtraction, reloop
    slt $t4, $s6, $zero
    bne $t4, $zero, addition_logic
    nop

    addu $t5, $s6, $a0
    slt $t4, $t5, $a0
    bne $t4, $zero, output_overflow_error
    nop

    beq $s5, $a0, output_overflow_error
    nop

    j valid_operator
    nop

multiplication:
    beq $s6, $zero, zero_multiplication
    nop

    slt $t4, $a0, $zero
    bne $t4, $zero, multiplication_logic
    nop

    mullo $t5, $s6, $a0

    slt $t4, $t5, $a0
    bne $t4, $s5, output_overflow_error
    nop

    j valid_operator
    nop

zero_multiplication:
    li $t5, 0
    j valid_operator
    nop

#>> Exit points

valid_operator:
    move $a0, $t5  # move value of tmp to $a0
    move $t2, $t1  # move current ascii operator to $t1
    li $s7, 1      # now we already have an operator
    li $s6, 0      # reset the current integer
    jr $ra         # return
    nop

set_flag_to_negative:
    li $s5, 1
    jr $ra
    nop

store_operator:
    move $a0, $s6  # the previous value will need to be stored
    move $t2, $t1  # store current operator ascii
    li $s6, 0      # reset the current integer
    li $s7, 1      # we now have an operator
    jr $ra
    nop

equals:
    push $ra
    jal handle_operator
    nop

    pop $ra
    call project3_output_number

    reset_values_and_exit:
        # im resetting everything because i got random errors and i dont
        # know which variable i changed to fix it

        li $t5, 0   # temp sum value
        li $t4, 0   # temp variable value
        li $s5, 0   # negative flag
        li $s7, 0   # already_have_operator flag
        li $t2, 0   # previous operator ascii
        li $t1, 0   # current  operator ascii
        li $s6, 0   # current integer
        li $a0, 0   # total value
        li $a1, 0   # error code
        li $t8, 0   # uart module error?
        li $t9, 0
        li $t6, 0


        li $s0, 0xF0000000	# UART
        li $s1, 0x1			# Mask for bit 0, used by put_char
        li $s2, 0x2			# Mask for bit 1, used by get_char
        li $s3, 10		# Used by decimal_to_binary
        li $s4, 48		# Subtract from ascii for decimal

    # return out
    j read_first_char
    nop
