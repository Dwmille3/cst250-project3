
.org 0x10000000

#>> Initializations
li $sp, 0x10fffffc

# UART Functions
li $s0, 0xF0000000		# UART
li $s1, 0x1			# Mask for bit 0, used by put_char
li $s2, 0x2			# Mask for bit 1, used by get_char
li $t4, 0			# temp variable value
li $t5, 0			# temp total value
li $t8, 0			# started finding an expression flag
li $s7, 0			# Flag to check if an operator was recently stored

# Conversion Constants
li $s3, 10		# Used by decimal_to_binary
li $s4, 48		# Subtract from ascii for decimal

# Calculator Values
li $t1, 0		# current ascii value
li $t2, 0		# previous ascii value
li $s5, 0 		# Sign bit
li $s6, 0		# Current number
li $a0, 0		# Total value
li $a1, 0		# Error code
#>> Main loop

read_first_char:
	jal get_char
	nop

	# not a valid expression
	li $t0, 61
	beq $t0, $t1, invalid_expression_error
	nop

	# if first char is '-', set sign bit ($s5) to 1
	li $t0, 45
	bne $t0, $t1, no_negative_sign
	nop

	li $s5, 1	# set sign bit to 1
	j get_num	# first character is negative sign, get next char
	nop

	no_negative_sign:
		jal parse
		nop

get_num:
	jal get_char
	nop

	parse:
		jal finding_expression
		nop

		jal is_number
		nop

		jal handle_char
		nop

		j get_num
		nop

finding_expression:
	li $t8, 1
	jr $ra
	nop

is_number:
	push $ra

	li $t0, 0x30		# ascii 0
	beq $t0, $t1, valid_input
	nop

	addiu $t0, $t0, 1	# acscii 1
	beq $t0, $t1, valid_input
	nop

	addiu $t0, $t0, 1	# acscii 2
	beq $t0, $t1, valid_input
	nop

	addiu $t0, $t0, 1	# acscii 3
	beq $t0, $t1, valid_input
	nop

	addiu $t0, $t0, 1	# acscii 4
	beq $t0, $t1, valid_input
	nop

	addiu $t0, $t0, 1	# acscii 5
	beq $t0, $t1, valid_input
	nop

	addiu $t0, $t0, 1	# acscii 6
	beq $t0, $t1, valid_input
	nop

	addiu $t0, $t0, 1	# acscii 7
	beq $t0, $t1, valid_input
	nop

	addiu $t0, $t0, 1	# acscii 8
	beq $t0, $t1, valid_input
	nop

	addiu $t0, $t0, 1	# acscii 9
	beq $t0, $t1, valid_input
	nop

	pop $ra
	jr $ra
	nop

valid_input:
	jal string_to_value
	nop

	li $s7, 0			# Reset operator load

	pop $ra
	j get_num
	nop

string_to_value:
	mulhi $t9, $s6, $s3
	bne $t9, $0, input_overflow_error	#result has caused an overflow
	nop

	mullo $s6, $s6, $s3	#multiply current result by 10
	srl $t9, $s6, 31 	#get sign bit
	beq $s1, $t9, input_overflow_error	#sign bit has been set, invalid_input
	nop

	subu $t1, $t1, $s4	#convert x from ascii to decimal
	addu $s6, $s6, $t1	#current number + x
	srl $t9, $s6, 31 	#get sign bit
	beq $s1, $t9, input_overflow_error	#sign bit has been set, invalid_input
	nop

	jr $ra
	nop

get_char:
	lw $t0, 4($s0)			# load status register
	and $t0, $t0, $s2		# Mask for ready bit

	# if currently finding an expression
	bne $t8, $zero, end_of_statement_check
	nop

	# if current number is not a zero
	bne $s6, $zero, end_of_statement_check
	nop

	# if ascii value is not a zero
	bne $t1, $zero, end_of_statement_check
	nop

	# if ascii value is equal to ascii zero
	li $t9, 48
	beq $t1, $t9, end_of_statement_check
	nop

	continue_reading_char:
		bne $t0, $s2, get_char
		nop

		lw $t1, 8($s0)			# load from recieve buffer
		sw $s2, 0($s0)			# Command register: clear status
		jr $ra
		nop

end_of_statement_check:
	bne $t0, $s2, invalid_expression_error
	nop

	j continue_reading_char
	nop

#>> Errors

error:
	li $t4, 61
	beq $t1, $t4, skip
	nop

	# no more input
	beq $t0, $zero, skip
	nop

	# Clear the rest of the input
	jal get_char
	nop

	bne $t1, $t4, error
	nop

	# Reset all values, restart loop
	skip:
		call project3_output_number
		j reset_values_and_exit
		nop

invalid_char_error:
	li $a1, 1
	j error
	nop

invalid_expression_error:
	li $a1, 2
	j error
	nop

input_overflow_error:
	li $a1, 3
	li $t0, -2147483648
	beq $s6, $t0, input_check
	nop

	j error
	nop

input_check:
	bne $t9, $s5, error
	nop
	
	li $a1, 0
	jr $ra
	nop

output_overflow_error:
	li $a1, 4
	j error
	nop

unknown_code_error:
	li $a1, 5
	j error
	nop
